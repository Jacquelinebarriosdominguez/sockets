#include <socket_server.h>
#include <socket.h>
#include <string.h>
#include <stdio.h>

main ()
{
	int Socket_Servidor;
	int Socket_Cliente;
	char Cadena[100];

	Socket_Servidor = Abre_Socket_Inet ("cpp_local");
	if (Socket_Servidor == -1)
	{
		printf ("No se puede abrir socket servidor\n");
		exit (-1);
	}
	Socket_Cliente = Acepta_Conexion_Cliente (Socket_Servidor);
	if (Socket_Servidor == -1)
	{
		printf ("No se puede abrir socket de cliente\n");
		exit (-1);
	}

	Lee_Socket (Socket_Cliente, Cadena, 25);

	printf ("Soy Servior, he recibido : %s\n", Cadena);

	strcpy (Cadena, "Bye... Vuelva pronto");
	Escribe_Socket (Socket_Cliente, Cadena, 25);

	close (Socket_Cliente);
	close (Socket_Servidor);
}

